/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cajero.modelos;

import com.cajero.persistencia.Conexion;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Tefa
 */
public class Cuenta {
    
    // Atributos
    private int id;
    private int numero_cuenta;
    private int cvv;
    private int clave;
    private float saldo;
    private int idEstudiante;
    
    // Constructor
    public Cuenta(int id, int numero_cuenta, int cvv, int clave, float saldo){
        super(); // Hereda de la clase principal
        this.id = id;
        this.numero_cuenta = numero_cuenta;
        this.cvv = cvv;
        this.clave = clave;
        this.saldo = saldo;
    }
    
    public Cuenta(){
        
    }

    // Métodos
    // obtener / consultar
    public int getId() {
        return id;
    }
    
    // set - modificar
    public void setId(int id) {
        this.id = id;
    }
    
    public float getNumero_cuenta() {
        return numero_cuenta;
    }
    
    public void setNumero_cuenta(int numero_cuenta) {
        this.numero_cuenta = numero_cuenta;
    }
    
    public int getCvv() {
        return cvv;
    }
    
    public void setCvv(int cvv) {
        this.cvv = cvv;
    }
    
    public int getClave() {
        return clave;
    }
    
    public void setClave(int clave) {
        this.clave = clave;
    }
    
    public float getSaldo() {
        return saldo;
    }
    
    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
    
    public int getIdEstudiante(){
        return this.idEstudiante;
    }
    
    public void setIdEstudiante(int id_estudiante){
        this.idEstudiante = id_estudiante;
    }
    
    // CRUD
    // Guardar
    public void guardar() throws ClassNotFoundException, SQLException{
        String  sql = "INSERT INTO cuenta(id, numero_cuenta, cvv, clave) VALUES("+getId()+","+getNumero_cuenta()+","+getCvv()+","+getClave()+");";
        Conexion.ejecutarConsulta(sql);
    }
    
    // Consultar
    public boolean consultar() throws ClassNotFoundException, SQLException{
        //CRUD -R 
        String  sql = "SELECT id,numero_cuenta,cvv,clave,saldo,id_estudiante FROM cuentas WHERE id_estudiante = '" + this.getIdEstudiante()+"'";
        ResultSet rs = Conexion.ejecutarConsulta(sql);
        while(rs.next()) // Mientras se traiga todos los datos del id_estudiante => Cursor.
        {
          this.setId(rs.getInt("id")); // => Cursor
          this.setClave(rs.getInt("clave"));  // => Cursor
          this.setCvv(rs.getInt("cvv"));  // => Cursor
          this.setNumero_cuenta(rs.getInt("numero_cuenta"));  // => Cursor
          this.setSaldo(rs.getFloat("saldo"));  // => Cursor
          this.setIdEstudiante(rs.getInt("id_estudiante"));  // => Cursor -> foreing key 
        }
        
        if (this.getNumero_cuenta() != 0){
            return true;
        } else {
            return false;
        }
    }
    
    // Actualizar 
    public void actualizar() throws ClassNotFoundException, SQLException{
        // CRUD -U
        String sql = "UPDATE cuentas SET numero_cuenta = "+getNumero_cuenta()+",cvv = "+getCvv()+", clave = "+getClave()+",saldo = "+getSaldo()+"WHERE id ="+getId()+";";
        Conexion.ejecutarConsulta(sql);
    }
    
    // Borrar
    public void borrar() throws SQLException, ClassNotFoundException{
        //CRUD -D
        String sql = "DELETE FROM cuentas WHERE id ="+getId()+";";
        Conexion.ejecutarConsulta(sql);
    }

    // Sobre escribir el metodo ToString para ver los valor de los atributos de una clase
    @Override
    public String toString() {
        return "Cuenta{" + "id=" + id + ", numero_cuenta=" + numero_cuenta + ", cvv=" + cvv + ", clave=" + clave + ", saldo="+ saldo + '}';
    }    
}
